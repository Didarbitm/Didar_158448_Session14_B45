<?php


namespace App;


class Calculetor
{

    private $number1;
    private $number2;
    private $oparation;

    private $result;


    public function setNumber1($number1)
    {
        $this->number1 = $number1;
    }

    public function setNumber2($number2)
    {
        $this->number2 = $number2;
    }

    public function setOperation($oparation)
    {
        $this->oparation = $oparation;
    }


    public function getNumber1()
    {
        return $this->number1;
    }


    public function getNumber2()
    {
        return $this->number2;
    }


    public function getOparation()
    {
        return $this->oparation;
    }


    public function setResult()
    {
        switch($this->oparation){

            case "Addition":
            $this->result = $this->doAddtion();
            break;
            case "Subtraction":
            $this->result = $this->doSubtraction();
            break;
            case "Multipication":
            $this->result = $this->doMultipication();
            break;
            case "Divition":
            $this->result = $this->doDivition();
        }

    }

    public function getResult()
    {
        $this->setResult();
        return $this->result;
    }


    private function doAddtion(){

        return $this->number1 + $this->number2;
    }


    private function doSubtraction(){

        return $this->number1 - $this->number2;
    }


    private function doMultipication(){

        return $this->number1 * $this->number2;
    }

    private function doDivition(){

        return $this->number1 / $this->number2;
    }

}